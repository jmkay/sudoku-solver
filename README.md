# README #

Makes use of a recursive backtracking algorithm to solve valid Sudoku challenges entered via a 2D matrix

![picture](/sudoku-screenshot.png)
