const unsolvedChallenge = [
  [4, null, null, null, 7, null, 2, null, null],
  [null, null, null, null, null, null, 6, 4, null],
  [null, 6, null, null, null, null, null, 5, null],
  [null, null, null, 1, null, null, null, null, 5],
  [null, null, null, null, null, null, null, null, null],
  [null, 7, 8, null, null, null, null, null, null],
  [null, 3, null, null, null, null, null, 8, 4],
  [7, null, null, null, null, null, 3, null, null],
  [5, null, null, null, null, null, null, null, null]
]

console.log('Unsolved challenge:')
printGame(unsolvedChallenge);
console.log('')
console.log('Solution:')
solveGame(unsolvedChallenge);

function solveGame(game) {
  let emptySquare = findEmptySquare(game);
  if (emptySquare === undefined) {
    printGame(game);
    return true;
  }

  for (let n = 1; n <= 9; n++) {
    if (validate(game, n, emptySquare)) {
      game[emptySquare[0]][emptySquare[1]] = n;

      if (solveGame(game)) {
        return true;
      }
    }

    game[emptySquare[0]][emptySquare[1]] = null;
  }

  return false;
}

function findEmptySquare(game) {
  let rowNum = 0;
  let colNum;
  for (let row of game) {
    colNum = row.findIndex(item => item === null);
    if (colNum >= 0) return [rowNum, colNum];
    rowNum++;
  }
  return undefined;
}

function validate(game, n, emptySquare) {
  game[emptySquare[0]][emptySquare[1]] = n;
  let updatedGame = game

  // Check row
  if (updatedGame[emptySquare[0]].filter(item => item === n).length !== 1) {
    return false;
  }

  // Check column
  let columnNums = [];
  for (let row = 0; row < updatedGame.length; row++) {
    columnNums.push(updatedGame[row][emptySquare[1]])
  }

  if (columnNums.filter(item => item === n).length !== 1) {
    return false;
  }

  // Check group of 9 squares
  let squareGroupOrigin = [emptySquare[0] - emptySquare[0] % 3, emptySquare[1] - emptySquare[1] % 3];
  let numsInSquareGroup = [];
  for (let row = squareGroupOrigin[0]; row < squareGroupOrigin[0] + 3; row++) {
    for (let col = squareGroupOrigin[1]; col < squareGroupOrigin[1] + 3; col++ ) {
      numsInSquareGroup.push(updatedGame[row][col]);
    }
  }

  if (numsInSquareGroup.filter(item => item === n).length !== 1) {
    return false;
  }

  return true;
}

function printGame(game) {
  let rowNum = 0;
  for (let row of game) {
    if (rowNum > 0 && rowNum % 3 === 0) console.log("- - - | - - - | - - - ");
    console.log(
      row
        .map(item => item === null ? " " : item)
        .map((item, index) => index > 0 && index % 3 === 0 ? `| ${item}` : item)
        .join(' ')
    );

    rowNum++;
  }

  console.log('')
}
